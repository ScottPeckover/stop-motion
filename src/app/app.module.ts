import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCamera, faExpand, faFileUpload, faPause, faPlay, faSave, faTrash } from '@fortawesome/free-solid-svg-icons';
import { WebcamModule } from 'ngx-webcam';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    WebcamModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faPlay, faPause, faCamera, faSave, faFileUpload, faTrash, faExpand);
  }
}
