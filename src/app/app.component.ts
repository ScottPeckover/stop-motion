import { Component, OnInit } from '@angular/core';
import { saveAs } from 'file-saver';
import { WebcamInitError } from 'ngx-webcam';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  frames = [];
  selectedFrameIndex: number;
  captureFrame: Subject<void> = new Subject();
  playerInterval: any;
  isPlaying: boolean;
  fps = 12;
  width: number;
  height: number;

  ngOnInit() {
    this.width = screen.availWidth / 2;
    this.height = screen.availHeight / 2;
    document.addEventListener('keydown', event => {
      if (this.selectedFrameIndex !== undefined) {
        switch (event.key) {
          case 'ArrowLeft': this.selectedFrameIndex = this.selectedFrameIndex === 0 ? this.frames.length - 1 : this.selectedFrameIndex -= 1;
            break;
          case 'ArrowRight': this.incrementSelectedFrame();
            break;
          case 'Delete': this.delete();
        }
      }
    });
    document.addEventListener('fullscreenchange', () => {
      this.width = document.fullscreen ? screen.availWidth : screen.availWidth / 2;
      this.height = document.fullscreen ? screen.availHeight : screen.availHeight / 2;
    });
  }

  addFrame(frame: string) {
    if (this.selectedFrameIndex !== undefined && this.selectedFrameIndex + 1 < this.frames.length) {
      this.frames = [...this.frames.slice(0, this.selectedFrameIndex + 1), frame, ...this.frames.slice(this.selectedFrameIndex + 1)];
    } else {
      this.frames.push(frame);
    }
  }

  incrementSelectedFrame() {
    this.selectedFrameIndex = this.selectedFrameIndex === this.frames.length - 1 ? 0 : this.selectedFrameIndex += 1;
  }

  play() {
    this.stop();
    if (this.selectedFrameIndex === undefined) {
      this.selectedFrameIndex = 0;
    }
    this.playerInterval = setInterval(() => {
      this.incrementSelectedFrame();
    }, 1000 / this.fps);
    this.isPlaying = true;
  }

  stop() {
    if (typeof this.playerInterval === 'number') {
      clearInterval(this.playerInterval);
      this.isPlaying = false;
    }
  }

  handleInitError(error: WebcamInitError): void {
    if (error.mediaStreamError && error.mediaStreamError.name === "NotAllowedError") {
      console.warn("Camera access was not allowed by user!");
    }
  }

  save() {
    const framesData = JSON.stringify({ frames: this.frames, fps: this.fps });
    const blob = new Blob([framesData], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "animation.frames");
  }

  load(event: HTMLInputElement) {
    event.files.item(0).text().then((res: string) => {
      const framesData = JSON.parse(res);
      this.frames = framesData.frames;
      this.fps = framesData.fps;
    });
  }

  delete() {
    this.frames.splice(this.selectedFrameIndex, 1);
    if (this.selectedFrameIndex >= this.frames.length) {
      if (this.frames.length === 0) {
        this.selectedFrameIndex = undefined;
      } else {
        this.selectedFrameIndex -= 1;
      }
    }
  }

  fullScreen() {
    if (!document.fullscreen) {
      const elem = document.getElementsByTagName('webcam').item(0) as any;
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
      }
    }
  }

}
